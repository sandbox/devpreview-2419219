<?php

/**
 * @file
 * Contains \Drupal\views_entity_form\Entity\Render\RendererBase.
 */

namespace Drupal\views_entity_form\Entity\Render;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Drupal\views_entity_form\Form\FormState;

/**
 * Defines a base class for entity row renderers.
 */
abstract class RendererBase {

  /**
   * The view executable wrapping the view storage entity.
   *
   * @var \Drupal\views\ViewExecutable
   */
  public $view = NULL;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  public $formBuilder;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The type of the entity being rendered.
   *
   * @var string
   */
  protected $entityType;

  /**
   * Contains an array of render arrays, one for each rendered entity.
   *
   * @var array
   */
  protected $build = array();

  /**
   * Constructs a renderer object.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The entity row being rendered.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   */
  public function __construct(ViewExecutable $view, FormBuilderInterface $form_builder, LanguageManagerInterface $language_manager, EntityTypeInterface $entity_type) {
    $this->view = $view;
    $this->formBuilder = $form_builder;
    $this->languageManager = $language_manager;
    $this->entityType = $entity_type;
  }

  /**
   * Alters the query if needed.
   *
   * @param \Drupal\views\Plugin\views\query\QueryPluginBase $query
   *   The query to alter.
   */
  public function query(QueryPluginBase $query) {
    
  }

  /**
   * Runs before each row is rendered.
   *
   * @param $result
   *   The full array of results from the query.
   */
  public function preRender(array $result, $trait) {
    /* @var $entityManager \Drupal\Core\Entity\EntityManager */
    $entityManager = $this->view->rowPlugin->entityManager;
    if(!$entityManager) {
        $entityManager = \Drupal::entityManager();
    }

    /* @var $row \Drupal\views\ResultRow */
    foreach ($result as $row) {
      $entity = $row->_entity;
      $form_mode = $trait->options['form_mode'];
      $base_form = $trait->options['base_form'];
      if ($base_form) {
        $entity->getEntityType()->setFormClass($form_mode, $entity->getEntityType()->getFormClass($base_form));
      }
      $form = $entityManager->getFormObject($entity->getEntityTypeId(), $form_mode);
      $form->setEntity($entity);
      $form_state = new FormState;
      if ($redirect = $trait->getEnforcedRedirect($entity)) {
        $form_state->setEnforcedRedirectUrl($trait->options['redirect_url']);
      }
      $this->build[$entity->id()] = $this->formBuilder->buildForm($form, $form_state);
    }
  }

  /**
   * Renders a row object.
   *
   * @param \Drupal\views\ResultRow $row
   *   A single row of the query result.
   *
   * @return array
   *   The renderable array of a single row.
   */
  public function render(ResultRow $row) {
    $entity_id = $row->_entity->id();
    return $this->build[$entity_id];
  }

  public function renderCurrent() {
    return current($this->build);
  }

}
