<?php

/**
 * @file
 * Contains \Drupal\views_entity_form\Plugin\Derivative\EntityFormRow.
 */

namespace Drupal\views_entity_form\Plugin\Derivative;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\views\ViewsData;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides views row plugin definitions for all non-special entity types.
 *
 * @ingroup views_row_plugins
 *
 * @see \Drupal\views_entity_form\Plugin\views\row\EntityFormRow
 */
class EntityFormRow implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * Stores all entity row plugin information.
   *
   * @var array
   */
  protected $derivatives = array();

  /**
   * The base plugin ID that the derivative is for.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The views data service.
   *
   * @var \Drupal\views\ViewsData
   */
  protected $viewsData;

  /**
   * Constructs a ViewsEntityRow object.
   *
   * @param string $base_plugin_id
   *   The base plugin ID.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\views\ViewsData $views_data
   *   The views data service.
   */
  public function __construct($base_plugin_id, EntityManagerInterface $entity_manager, ViewsData $views_data) {
    $this->basePluginId = $base_plugin_id;
    $this->entityManager = $entity_manager;
    $this->viewsData = $views_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id, $container->get('entity.manager'), $container->get('views.views_data')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinition($derivative_id, $base_plugin_definition) {
    if (!empty($this->derivatives) && !empty($this->derivatives[$derivative_id])) {
      return $this->derivatives[$derivative_id];
    }
    $this->getDerivativeDefinitions($base_plugin_definition);
    return $this->derivatives[$derivative_id];
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->entityManager->getDefinitions() as $entity_type_id => $entity_type) {
      // Just add support for entity types which have a views integration.
      if (($base_table = $entity_type->getBaseTable()) && $this->viewsData->get($base_table) && ($definition = $this->entityManager->getDefinition($entity_type_id, FALSE)) && $definition->hasFormClasses()
      ) {
        $this->derivatives[$entity_type_id] = array(
          'id' => $this->getId($entity_type_id),
          'provider' => 'views_entity_form',
          'base' => array($base_table),
          'entity_type' => $entity_type_id,
          'display_types' => array('normal'),
          'class' => $base_plugin_definition['class'],
          'title' => $this->getTitle($entity_type->getLabel())
        );
      }
    }
    return $this->derivatives;
  }

  protected function getId($entity_type_id) {
    return 'views_entity_form:' . $entity_type_id;
  }

  protected function getTitle($label) {
    return $this->t('!entity form', array('!entity' => $label));
  }

}
