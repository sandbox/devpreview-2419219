<?php

/**
 * @file
 * Contains \Drupal\views_entity_form\Plugin\views\area\EntityFormArea.
 */

namespace Drupal\views_entity_form\Plugin\views\area;

use Drupal\views\Plugin\views\area\AreaPluginBase;
use Drupal\views_entity_form\EntityFormTrait;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\views\ResultRow;
use Drupal\Core\Form\FormStateInterface;

/**
 * Alter the HTTP response status code used by the view.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("views_entity_form")
 */
class EntityFormArea extends AreaPluginBase {

  use EntityFormTrait {
    EntityFormTrait::buildOptionsForm as _buildOptionsForm;
    EntityFormTrait::defineOptions as _defineOptions;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityManagerInterface $entity_manager, FormBuilderInterface $form_builder, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->construct($entity_manager, $form_builder, $language_manager);
  }

  protected function defineOptions() {
    $options = $this->_defineOptions();
    $options['entity_id'] = array('default' => '');
    $options['entity_bundle'] = array('default' => 0);
    $options['entity_values'] = array('default' => '');
    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $this->_buildOptionsForm($form, $form_state);

    $bundles = array();
    foreach ($this->entityManager->getBundleInfo($this->entityType->id()) as $bundle => $data) {
      $bundles[$bundle] = $data['label'];
    }
    $form['entity_bundle'] = array(
      '#type' => 'select',
      '#title' => $this->entityType->getBundleLabel(),
      '#default_value' => $this->options['entity_bundle'],
      '#options' => array_merge(array(0 => $this->t('Not specified')), $bundles)
    );

    $form['entity_id'] = array(
      '#title' => $this->t('@entity_type_label ID', array('@entity_type_label' => $this->entityType->getLabel())),
      '#type' => 'textfield',
      '#default_value' => $this->options['entity_id'],
      '#description' => $this->t('If the @entity_type_label ID is empty, it will create a new @entity_type_label.', array('@entity_type_label' => $this->entityType->getLabel()))
    );

    $form['entity_values'] = array(
      '#title' => $this->t('@entity_type_label values', array('@entity_type_label' => $this->entityType->getLabel())),
      '#type' => 'textarea',
      '#default_value' => $this->options['entity_values'],
      '#description' => $this->t('If the @entity_type_label ID is empty, it will create a new @entity_type_label. New entity has have this values. Replace pattern: [arg_*] to patch args.', array('@entity_type_label' => $this->entityType->getLabel()))
    );
  }

  public function preRender(array $results) {
    parent::preRender($results);
    $results = array();
    if ($this->options['entity_id']) {
      if ($entity = $this->loadEntity($this->options['entity_id'])) {
        $row = new ResultRow(array('_entity' => $entity));
        $row->index = 0;
        $results[] = $row;
      }
    }
    else {
      $row = new ResultRow(array('_entity' => $this->createEntity()));
      $row->index = 0;
      $results[] = $row;
    }
    $this->getRenderer()->preRender($results, $this);
  }

  /**
   * {@inheritdoc}
   */
  function render($empty = FALSE) {
    return $this->getRenderer()->renderCurrent();
  }

  public function adminSummary() {
    $summary = $this->summaryTitle();
    if ($this->options['entity_id']) {
      $summary .= ', ID: ' . $this->options['entity_id'];
    }
    return $summary;
  }

  protected function createEntity(array $values = array()) {
    if ($this->options['entity_bundle']) {
      $values[$this->entityType->getKey('bundle')] = $this->options['entity_bundle'];
    }

    if ($this->options['entity_values']) {
      $entity_values = array();
      foreach (explode(PHP_EOL, $this->options['entity_values']) as $entity_value) {
        $tmp = explode('|', $entity_value);
        if (count($tmp) > 1) {
          $entity_value = $tmp[1];
          foreach (\Drupal::routeMatch()->getRawParameters()->all() as $arg => $arg_value) {
            $entity_value = str_replace('[' . $arg . ']', $arg_value, $entity_value);
          }
          $values[trim($tmp[0])] = trim($entity_value);
        }
      }
    }

    return $this->entityManager->getStorage($this->entityTypeId)->create($values);
  }

  protected function loadEntity($id) {
    return $this->entityManager->getStorage($this->entityTypeId)->load($id);
  }

}
