<?php

/**
 * @file
 * Definition of Drupal\views_entity_form\Plugin\views\row\EntityFormRow.
 */

namespace Drupal\views_entity_form\Plugin\views\row;

use Drupal\views\Plugin\views\row\RowPluginBase;
use Drupal\views_entity_form\EntityFormTrait;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;

/**
 * Row handler plugin for displaying entity form.
 *
 * @ViewsRow(
 *   id = "views_entity_form",
 *   title = @Translation("Form"),
 *   deriver = "Drupal\views_entity_form\Plugin\Derivative\EntityFormRow"
 * )
 */
class EntityFormRow extends RowPluginBase {

  use EntityFormTrait {
    EntityFormTrait::buildOptionsForm as _buildOptionsForm;
    EntityFormTrait::defineOptions as _defineOptions;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityManagerInterface $entity_manager, FormBuilderInterface $form_builder, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->construct($entity_manager, $form_builder, $language_manager);
  }

  protected function defineOptions() {
    $options = $this->_defineOptions();
    $options['entity_create'] = array('default' => 0);
    $options['entity_bundle'] = array('default' => 0);
    $options['entity_values'] = array('default' => '');
    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $this->_buildOptionsForm($form, $form_state);

    $form['entity_create'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Create new entity'),
      '#default_value' => $this->options['entity_create']
    );

    $bundles = array();
    foreach ($this->entityManager->getBundleInfo($this->entityType->id()) as $bundle => $data) {
      $bundles[$bundle] = $data['label'];
    }
    $form['entity_bundle'] = array(
      '#type' => 'select',
      '#title' => $this->entityType->getBundleLabel(),
      '#default_value' => $this->options['entity_bundle'],
      '#options' => array_merge(array(0 => $this->t('Not specified')), $bundles)
    );

    $form['entity_values'] = array(
      '#title' => $this->t('@entity_type_label values', array('@entity_type_label' => $this->entityType->getLabel())),
      '#type' => 'textarea',
      '#default_value' => $this->options['entity_values'],
      '#description' => $this->t('If the @entity_type_label ID is empty, it will create a new @entity_type_label. New entity has have this values.<br>Replace patterns:', array('@entity_type_label' => $this->entityType->getLabel()))
    );

    $form['entity_values']['#description'] .= '<ul><li>[arg_*] = patch args;</li>';
    foreach ($this->displayHandler->getFieldLabels() as $field => $label) {
      $form['entity_values']['#description'] .= '<li>[' . $field . '] = ' . $label . ';</li>';
    }
    $form['entity_values']['#description'] .= '</ul>';
  }

  /**
   * {@inheritdoc}
   */
  public function preRender($result) {
    parent::preRender($result);
    if ($this->options['entity_create']) {
      $row = new ResultRow(array('_entity' => $this->createEntity()));
      $row->index = 0;
      $result = array($row);
    }
    if ($result) {
      $this->getRenderer()->preRender($result, $this);
    }
  }

  /**
   * Overrides Drupal\views\Plugin\views\row\RowPluginBase::render().
   */
  public function render($row) {
    if ($this->options['entity_create']) {
      return $this->getRenderer()->renderCurrent();
    }
    else {
      return $this->getRenderer()->render($row);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();

    $form_mode = $this->entityManager
      ->getStorage('entity_form_mode')
      ->load($this->entityTypeId . '.' . $this->options['form_mode']);
    if ($form_mode) {
      $dependencies[$form_mode->getConfigDependencyKey()][] = $form_mode->getConfigDependencyName();
    }

    return $dependencies;
  }

  protected function createEntity(array $values = array()) {
    if ($this->options['entity_bundle']) {
      $values[$this->entityType->getKey('bundle')] = $this->options['entity_bundle'];
    }
    if ($this->options['entity_values']) {
      $entity_values = array();
      foreach (explode(PHP_EOL, $this->options['entity_values']) as $entity_value) {
        $tmp = explode('|', $entity_value);
        if (count($tmp) > 1) {
          $entity_value = $tmp[1];
          foreach (\Drupal::routeMatch()->getRawParameters()->all() as $arg => $arg_value) {
            $entity_value = str_replace('[' . $arg . ']', $arg_value, $entity_value);
          }
          if ($entity = current($this->view->result)->_entity) {
            $fields = $entity->toArray();
          }
          foreach ($this->view->field as $field => $value) {
            /* @var $value \Drupal\node\Plugin\views\field\Node */
            $entity_value = str_replace('[' . $field . ']', current($fields[$value->field])['value'], $entity_value);
          }
          $values[trim($tmp[0])] = trim($entity_value);
        }
      }
    }

    return $this->entityManager->getStorage($this->entityTypeId)->create($values);
  }

}
