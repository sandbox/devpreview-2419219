<?php

/**
 * @file
 * Contains \Drupal\views_entity_form\Form\FormState.
 */

namespace Drupal\views_entity_form\Form;

use Drupal\Core\Form\FormState as DrupalFormState;
use Drupal\Core\Url;

/**
 * Stores information about the state of a form.
 */
class FormState extends DrupalFormState {

  /**
   * Used to enforced redirect the form on submission.
   *
   * @see self::getEnforcedRedirect()
   *
   * This property is uncacheable.
   *
   * @var \Drupal\Core\Url|\Symfony\Component\HttpFoundation\RedirectResponse|null
   */
  protected $enforcedRedirect;

  /**
   * {@inheritdoc}
   */
  public function setEnforcedRedirect($route_name, array $route_parameters = array(), array $options = array()) {
    $url = new Url($route_name, $route_parameters, $options);
    return $this->setEnforcedRedirectUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public function setEnforcedRedirectUrl($url) {
    $this->enforcedRedirect = $url;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnforcedRedirect() {
    // Skip redirection for form submissions invoked via
    // \Drupal\Core\Form\FormBuilderInterface::submitForm().
    if ($this->isProgrammed()) {
      return FALSE;
    }
    // Skip redirection if rebuild is activated.
    if ($this->isRebuilding()) {
      return FALSE;
    }
    // Skip redirection if it was explicitly disallowed.
    if ($this->isRedirectDisabled()) {
      return FALSE;
    }

    $redirect = \Drupal\views_entity_form\EntityFormTrait::getEnforcedRedirectUrl($this->enforcedRedirect, $this->getFormObject()->getEntity()->id());
    if (!$redirect) {
      $redirect = false;
    }
    return $redirect;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirect() {
    if ($redirect = $this->getEnforcedRedirect()) {
      return $redirect;
    }
    return parent::getRedirect();
  }

}
