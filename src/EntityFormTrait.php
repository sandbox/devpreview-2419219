<?php

/**
 * @file
 * Contains \Drupal\views_entity_form\EntityFormTrait.
 */

namespace Drupal\views_entity_form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Component\Utility\String;

/**
 * 
 */
trait EntityFormTrait {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  public $entityManager;

  /**
   * Stores the entity type ID of the result entities.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * Contains the entity type of this row plugin instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * The table the entity is using for storage.
   *
   * @var string
   */
  public $base_table;

  /**
   * The actual field which is used for the entity id.
   *
   * @var string
   */
  public $base_field;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  public $formBuilder;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The renderer to be used to render the entity row.
   *
   * @var \Drupal\views\Entity\Rendering\RendererBase
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('entity.manager'), \Drupal::formBuilder(), $container->get('language_manager'));
  }

  protected function construct(EntityManagerInterface $entity_manager, FormBuilderInterface $form_builder, LanguageManagerInterface $language_manager) {
    $this->entityManager = $entity_manager;
    $this->formBuilder = $form_builder;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->entityTypeId = $this->definition['entity_type'];
    $this->entityType = $this->entityManager->getDefinition($this->entityTypeId);
    $this->base_table = $this->entityType->getBaseTable();
    $this->base_field = $this->entityType->getKey('id');
  }

  /**
   * Overrides Drupal\views\Plugin\views\row\RowPluginBase::defineOptions().
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['form_mode'] = array('default' => 'default');
    $options['base_form'] = array('default' => 0);
    $options['redirect_url'] = array('default' => '');
    return $options;
  }

  /**
   * Overrides Drupal\views\Plugin\views\row\RowPluginBase::buildOptionsForm().
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['form_mode'] = array(
      '#type' => 'select',
      '#required' => true,
      '#options' => $this->getEntityTypeFormModes(),
      '#title' => $this->t('Form mode'),
      '#default_value' => $this->options['form_mode']
    );

    $base_forms = array_merge(
      array(0 => $this->t('From form mode')), array_keys($this->entityManager->getDefinition($this->entityTypeId)->get('handlers')['form'])
    );
    $form['base_form'] = array(
      '#type' => 'select',
      '#required' => true,
      '#options' => array_merge(array(0 => $this->t('As form mode')), $this->getEntityTypeBaseForms()),
      '#title' => $this->t('Base form'),
      '#default_value' => $this->options['base_form'],
      '#element_validate' => array(array($this, 'validateBaseFormElement'))
    );

    $form['redirect_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Redirect URL'),
      '#default_value' => $this->options['redirect_url'],
      '#description' => $this->t('Replacement patterns: <ul><li>[eid] - entity id</li><li>&lt;front&gt; - front page</li><li>&lt;current&gt; - current form page</li><li>[arg_*] = patch args;</li></ul>'),
     // '#element_validate' => array(array($this, 'validateRedirectUrlElement'))
    );
  }

  /**
   * Get entity type form modes.
   * 
   * @return array
   */
  protected function getEntityTypeBaseForms() {
    $forms = array_keys($this->entityManager->getDefinition($this->entityTypeId)->get('handlers')['form']);
    return array_combine($forms, $forms);
  }

  /**
   * Get entity type form modes.
   * 
   * @return array
   */
  protected function getEntityTypeFormModes() {
    $result = $this->entityManager->getFormModeOptions($this->entityTypeId, true);
    return array_merge($this->getEntityTypeBaseForms(), $result);
  }

  public function validateRedirectUrlElement($element, FormStateInterface $form_state) {
    if (!empty($element['#value'])) {
      $url = $this->getEnforcedRedirectUrl($element['#value'], 0);
      if (!$url || !$url->isRouted()) {
        $form_state->setErrorByName('redirect_url', 'URL not found.');
      }
    }
  }

  public function validateBaseFormElement($element, FormStateInterface $form_state) {
    if ($form_state->hasValue('row_options')) {
      $form_mode = $form_state->getValue('row_options')['form_mode'];
    }
    else {
      $form_mode = $form_state->getValue('options')['form_mode'];
    }
    if (!$element['#value'] && !array_key_exists($form_mode, $this->getEntityTypeBaseForms())) {
      $form_state->setErrorByName('base_form', 'You must specify the base form.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEnforcedRedirect(EntityInterface $entity) {
    return $this->getEnforcedRedirectUrl($this->options['redirect_url'], $entity->id());
  }

  /**
   * 
   * @return Url
   */
  public static function getEnforcedRedirectUrl($redirect_url, $entity_id) {
    if (strlen($redirect_url) > 0) {
      if ($redirect_url == '<current>') {
        $redirect_url = \Drupal::request()->getRequestUri();
      }
      else {
        $redirect_url = str_replace('[eid]', $entity_id, $redirect_url);
        foreach (\Drupal::routeMatch()->getRawParameters()->all() as $arg => $arg_value) {
          $redirect_url = str_replace('[' . $arg . ']', $arg_value, $redirect_url);
        }
      }
      // Both PathValidator::getUrlIfValidWithoutAccessCheck() and 'base:' URIs
      // only accept/contain paths without a leading slash, unlike 'user-path:'
      // URIs, for which the leading slash means "relative to Drupal root" and
      // "relative to Symfony app root" (just like in Symfony/Drupal 8 routes).
      if ($redirect_url === '/') {
        $redirect_url = '<front>';
      }
      else {
        // Remove the leading slash.
        $redirect_url = substr($redirect_url, 1);
      }
      /* @var $url Url */
      $url = \Drupal::pathValidator()
          ->getUrlIfValidWithoutAccessCheck($redirect_url) ? : Url::fromUri('base:' . $redirect_url);
      return $url;
    }
    return null;
  }

  /**
   * Overrides Drupal\views\Plugin\views\PluginBase::summaryTitle().
   */
  public function summaryTitle() {
    if (array_key_exists($this->options['form_mode'], $this->getEntityTypeFormModes())) {
      if ($this->options['redirect_url']) {
        return String::checkPlain($this->getEntityTypeFormModes()[$this->options['form_mode']] . ', redirect: ' . $this->options['redirect_url']);
      }
      else {
        return String::checkPlain($this->getEntityTypeFormModes()[$this->options['form_mode']]);
      }
    }
    else {
      return $this->t('No form mode selected');
    }
  }

  /**
   * Returns the current renderer.
   *
   * @return \Drupal\views\Entity\Render\RendererBase
   *   The configured renderer.
   */
  protected function getRenderer() {
    if (!isset($this->renderer)) {
      //$class = '\Drupal\views_entity_form\Entity\Render\\' . Container::camelize($this->displayHandler->getOption('rendering_language'));
      $class = '\Drupal\views_entity_form\Entity\Render\DefaultLanguageRenderer';
      $this->renderer = new $class($this->view, $this->formBuilder, $this->languageManager, $this->entityType);
    }
    return $this->renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    parent::query();
    $this->getRenderer()->query($this->view->getQuery());
  }

}
